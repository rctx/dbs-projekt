Wir haben diskutiert, welche mögliche Dimensionen und Metriken wir für die Wichtigkeit bzw Ähnlichkeit von Hashtags benutzen können. Möglichkeiten waren folgende:

* Favoritenanzahl
* Retweets
* Häufigket
* Länge
* Zeit
    * erste Nutzung
    * Zeitraum, wann es benutzt wird

Die Vermutung war, dass es eine Korrelation zwischen die Anzahl von Retweets und die Anzahl von Favorites existiert. Falls das der Fall ist, können wir die beide Zahlen aufsummieren und die Summe als eine einzige Metrik betrachten. Um diese Hypothese zu testen haben wir ein Programm in Python geschrieben, was die Pearson-Korrelation berechnet. Je näher das Ergebnis zu -1 oder +1, desto stärker ist die Korrelation zwischen die zwei Dimensionen. Zusätzlich gibt es auch den p-Wert, der zeigt, wie wahrscheinlich ist es, dass die Nullhypothese (in diesem Fall, dass es keine Korrelation zwischen Retweets und Favorites gibt) gilt. Als Datensatz haben wir die Excel-Tabelle benutzt, die durch Daten-Bereinigung erzeugt wird.

    Pearson correlation coefficient:
    r =  0.927321663797
    p-value =  0.0

![Korrelation zwischen Retweets und Favorites in Tweets](https://git.imp.fu-berlin.de/rctx/dbs-projekt/raw/draft/data-mining/korr_1.png)

Mit dem Test haben wir festgestellt, dass es eine starke Korrelation zwische Retweets und Favorites gibt. Wir haben auch bemerkt, dass es gibt ein Tweet mit extrem vielen Retweets und Favorites. Wir haben nochmal das Programm ausgeführt und die Pearson-Korrelation berechnet, ohne diesen Sonderfall.

    Pearson correlation coefficient:
    r =  0.932610029559
    p-value =  0.0

![Korrelation zwischen Retweets und Favorites in Tweets (ohne Outlier)](https://git.imp.fu-berlin.de/rctx/dbs-projekt/raw/draft/data-mining/korr_2.png)