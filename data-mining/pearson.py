import openpyxl #used to handle Excel Worksheets
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from scipy import stats

wb = openpyxl.load_workbook('cleaned-tweets.xlsx') #opens our Excel Workbook
sheet = wb.get_sheet_by_name('cleaned-tweets') #retrieves the specific Sheet we want

retweets = []
favourites = []

#the for loop iterates through every row and initiates a number of actions
for i in range(2,sheet.max_row+1):
	#print("Processing entry "+str(i-1)) <-- used only to track and potentially debug our code
	tweet_rts = sheet.cell(row=i, column=3).value
	tweet_favs = sheet.cell(row=i, column=4).value
	retweets.append(tweet_rts)
	favourites.append(tweet_favs)
	### folgende Zeilen werden benutzt anstatt die letzte zwei zeilen, nur um Pearson-Korrelation ohne Sonderfall zu berechnen
#	if tweet_rts < 300000:
#		retweets.append(tweet_rts)
#		favourites.append(tweet_favs)

rtarray = np.array(retweets)
fvarray = np.array(favourites)

# Erzeugt unser Graph mit Line of Best Fit
plt.scatter(rtarray, fvarray, color='black', s=5)
plt.plot(np.unique(rtarray), np.poly1d(np.polyfit(rtarray, fvarray, 1))(np.unique(rtarray)), color='blue', linewidth=2)

#calculates and returns Pearson correlation coefficient
r, pval = stats.pearsonr(rtarray,fvarray)
print("Pearson correlation coefficient:")
print("r = ",r)
print("p-value = ",pval)

plt.title("Korrelation zwischen Retweets und Favorites in Tweets")
plt.xlabel("Anzahl Retweets")
plt.ylabel("Anzahl Favorites")
plt.show()