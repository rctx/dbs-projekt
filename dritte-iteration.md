
TO-DO:

- wichtigste Tweets (Retweets, Favorites) -> Liste aus Tweets [[ID, Text, Zeitunkt, Autor, Retweets, Favorites, Text], ...]
- am meisten verwendete Hashtags-> Liste aus Hashtags, sortiert [[Hashtag, #Verwednung], ...]
- Häufigkeit des Aufrtetens von Hashtags pro Tag -> Liste [[Tag "YYYY-MM-DD", #Anzahl], ...] -> Balkendiagram, für jeden Tag (3/2.4)
- am meisten gemeinsam auftertende Hashtags -> Liste, sortiert  [[HashtagA, HashtagB, #Verwednung], ..]
- Häufigkeit eines speziellen Hashtags über die Zeit -> Dict {Hashtag1 :  [Tag "YYYY-MM-DD",  #Anzahl], ...} -> Balkendiagram, für jeden Tag (3/2.5)

- Clusteranalyse -> -> Koordinatensystem mit Farben?   (3/1)
    - mögliche Metriken:
        - Favoritenanzahl
        - Retweets
        - Länge
        - Zeit

- gemeinsam auftertende Hashtaags -> Netzwerk (3/2.2)
    - Dateneabfrage siehe oben
- gemeinsam auftertende Hashtags mit Ähnlichkeit -> farbiges Netzwerk (3/2.3)
