
// erzeuge Balkendiagramm
$.getJSON("data/barchart-hashtag-all.json", function(json) {
  data = [];

  /* erzeuge Arrays mit den Wertepaaren */
  for (i = 0; i<json.data.length; i ++) {
      /* erzeuge den UNIX-Timestamp in Millisekunden */
      data.push([new Date(json.data[i][0]).getTime(), json.data[i][1]]);
  }

  /* barchart */
  $('#barchart-hashtag-all').css('width', 'max').css('height', '400px');
  $.plot(
    $("#barchart-hashtag-all"),
    [ data ],
    {
      xaxis: {
          mode: "time",
          timeformat: "%Y/%m/%d",
          min: (new Date(json.min)).getTime(),
          max: (new Date(json.max)).getTime()
      },
      series: {
          bars: {
              show: true
          }
      },
    }
  );
});

// erzeuge cluster informationen
$.getJSON("data/network-hashtag-cluster.json", function(json) {
  $.each(json, function(i, cluster) {
    $("#list-quickfacts-cluster").append(
      "<li class=\"list-group-item\">" + cluster.hashtags.length
      + " hashtags in cluster "
      + "<font color=\"" + cluster.color + "\">\"" + cluster.name + "\"</font>"
      + "</li>");
  })
})

// erzeuge Liste mit quickfacts
$.getJSON("data/list-quickfacts.json", function(json){
  $.each(json, function(i, elem){
    $("#list-quickfacts").append("<li class=\"list-group-item\">" + elem + "</li>");
  })
});

// erzeuge hashtag netzwerk
var network = new sigma('network-hashtag');
network.settings({
  defaultNodeColor: '#337ab7',
  defaultEdgeColor: '#ddd',
  edgeColor: 'default'
});
sigma.parsers.json(
  'data/network-hashtag.json',
  network,
  function() {
    network.refresh();
  }
);

// aktiviere Anzeige der Cluster
function networkClusterEnable() {
  $.getJSON("data/network-hashtag-cluster.json", function(json){
    $.each(json, function (i, cluster) {
      $.each(cluster.hashtags, function(i, hashtag){
        network.graph.nodes(hashtag).color = cluster.color;
      })
      $.each(network.graph.edges(), function(i, edge){
        if (($.inArray(edge.source, cluster.hashtags) >= 0) && ($.inArray(edge.target, cluster.hashtags) >= 0 )) {
          edge.color = cluster.colorEdge;
        }
      })
    })
    network.refresh();
  })
}

// deaktiviere Anzeige der Cluster
function networkClusterDisable() {
  $.each(network.graph.edges(), function(i, edge) {
    edge.color = null;
  })
  $.each(network.graph.nodes(), function(i, node) {
    node.color = null;
  })
  network.refresh();
}

// erzeuge Tabelle mit allen Hashtags
$.getJSON("data/hashtag-all.json", function(json) {
  $.each(json, function(i, elem){
    $("#hashtag-all tbody").append("<tr><td>" + elem.ranking + "</td><td>" + elem.count + "</td><td>" + linkHashtag(elem.hashtag) + "</td></tr>");
  });
  $('#hashtag-all').DataTable();
      $(document).ready(function() {
  });
});

// erzeuge Tabelle mit Hashtagpaaren
$.getJSON("data/hashtagpairs-all.json", function(json) {
  $.each(json, function(i, elem){
    $("#hashtagpairs-all tbody").append("<tr><td>" + elem.ranking + "</td><td>" + elem.count + "</td><td>" + linkHashtag(elem.texta) + ", " + linkHashtag(elem.textb) + "</td></tr>");
  });
   $('#hashtagpairs-all').DataTable();
       $(document).ready(function() {
  });
});
