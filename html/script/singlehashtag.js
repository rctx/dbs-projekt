// aktiviere event bei einem Hashtaglink
function activateHashtag(hashtag){
  return "<a class='activate-hashtag' onclick='sethashtag(\"" + hashtag + "\")'>#" + hashtag + "</a>";
}

// aktiviere Event bei einem Hashtaglink in einem Tweet
function activateHashtagInTweet(hashtag, normalizedhashtag){
  return "<a class='activate-hashtag' onclick='sethashtag(\"" + normalizedhashtag + "\")'>#" + hashtag + "</a>";
}

// Funktion ändert den Hashtag der Seite
function sethashtag(singlehashtag) {
  // setzte Übeschriften
  $('#header-information').html('<h1>Information about ' + activateHashtag(singlehashtag) + '</h1>');
  $('#header-day').html('<h4>Use of ' + activateHashtag(singlehashtag) + ' per day</h4>');
  $('#header-cluster').html('<h4>Cluster of ' + activateHashtag(singlehashtag) + ' <small>and quickfacts</small></h4>');
  $('#header-tweet').html('<h1>Tweets in that ' + activateHashtag(singlehashtag) + ' occours</h1>');
  $('#navbar .nav').html('<li><a href="hashtag.html">Hashtags</a></li><li><a href="tweet.html">Tweets</a></li>');
  $('#navbar .nav').append('<li class="active">'+ linkHashtag(singlehashtag) +  '</li>');

  // erstelle Balkendiagramm
  $.getJSON("data/barchart-hashtag.json", function(json) {
    //console.log(json);
    var data = [];
    var hashtagdata = json[singlehashtag].data;
    var hashtagmin = json[singlehashtag].min;
    var hashtagmax = json[singlehashtag].max;

    /* erzeuge Arrays mit den Wertepaaren */
    for (i = 0; i<hashtagdata.length; i ++) {
        /* erzeuge den UNIX-Timestamp in Millisekunden */
        data.push([new Date(hashtagdata[i][0]).getTime(), hashtagdata[i][1]]);
    }
    $('#header-day').html(
      '<h4>Use of  ' + activateHashtag(singlehashtag)
      + ' per day <small>from 2016/01/05 to 2016/09/27</small></h4>');

    /* barchart */
    $('#barchart-hashtag').css('width', 'max').css('height', '400px');
    $.plot(
      $("#barchart-hashtag"),
      [ data ],
      {
        xaxis: {
            mode: "time",
            timeformat: "%Y/%m/%d",
            min: (new Date(hashtagmin)).getTime(),
            max: (new Date(hashtagmax)).getTime()
        },
        series: {
            bars: {
                show: true
            }
        },
      }
    );
  });

  // erzeuge quickfact Liste
  $.getJSON("data/list-quickfacts-hashtag.json", function(json){
    hashtag = json[singlehashtag];
    $("#list-quickfacts-hashtag").html('<li  class="list-group-item disabled">Quickfacts</li>');
    $("#list-quickfacts-hashtag").append("<li class=\"list-group-item\">" + activateHashtag(singlehashtag) + " occurs in " + hashtag.tweets + " tweets.</li>")
    if(hashtag.hashtags.length == 0) {
      $("#list-quickfacts-hashtag").append("<li class=\"list-group-item\">" + activateHashtag(singlehashtag)  + " does not occur with any other hashtag</li>");
    } else if (hashtag.hashtags.length == 1){
      $("#list-quickfacts-hashtag").append("<li class=\"list-group-item\">" + activateHashtag(singlehashtag)  + " occurs with " + activateHashtag(hashtag.hashtags[0]) + ".</li>");
    } else {
      var s = activateHashtag(singlehashtag)  + " occurs with ";
      $.each(hashtag.hashtags, function(i, elem){
        if (i == hashtag.hashtags.length - 1){
          s += activateHashtag(elem) + ".";
        } else if (i == hashtag.hashtags.length - 2){
          s += activateHashtag(elem) + " and ";
        } else {
            s += activateHashtag(elem) + ", ";
        }
      });
      $("#list-quickfacts-hashtag").append("<li class=\"list-group-item\">" + s + "</li>");
    }
    $("#list-quickfacts-hashtag").append(
      "<li class=\"list-group-item\">" + activateHashtag(singlehashtag) + " is a member of the cluster "
      + "<font color=\"" + hashtag.clustercolor + "\">\"" + hashtag.clustername + "\"</font>"
      + ".</li>"
    );
  });

  // erzeuge Tweet Tabelle für den Hashtag
  $.getJSON("data/table-tweet-hashtag-all.json", function(json) {
    $('#table-tweet-hashtag-all tbody').html('');
    $.each(json[singlehashtag], function(i, elem){
      var row =  $('<tr></tr>');

      row.append('<td>' + elem.ranking + '</td>');

      var cell = $('<td></td>');
      var media = $('<div class="media"></div>');
      var medialeft = $('<div class="media-left"></div>');
      var mediabody = $('<div class="media-body"></div>');
      //medialeft.append('<img class="media-object" src="https://twitter.com/' + elem.autor + '/profile_image?size=bigger">');
      mediabody.append('<h4 class="media-heading">' + elem.autorname + ' <small>@' + elem.autor + '</small></h4>');
      var text = elem.text;
      $.each(elem.hashtags, function(i, normalizedhashtag) {
        var hregex = new RegExp('#' + normalizedhashtag, 'i');
        var hit = text.match(hregex);
        if (hit != null) {
          $.each(hit, function(i, hashtag){
            text = text.replace(hashtag, activateHashtagInTweet(hashtag.replace('#', ''), normalizedhashtag));
          });
        }
      });
      mediabody.append(text);

      media.append(medialeft);
      media.append(mediabody);
      cell.append(media);
      row.append(cell);

      row.append('<td>' + new Date(elem.zeitpunkt).toISOString() + '</td>');
      row.append('<td>' + elem.favourites + '</td>');
      row.append('<td>' + elem.retweets + '</td>');

      $('#table-tweet-hashtag-all tbody').append(row);
    });
    $('#table-tweet-hashtag-all').DataTable();
    $(document).ready(function() { });
  });
}
