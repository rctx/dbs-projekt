// verlinke eine hashtag im Tweet
function linkHashtagInTweet(hashtag, normalizedhashtag){
  return "<a href='singlehashtag.html?h=" + normalizedhashtag + "'>#" + hashtag + "</a>";
}

// verlinke eine hashtag im in einer Liste
function linkHashtag(hashtag){
  return "<a href='singlehashtag.html?h=" + hashtag + "'>#" + hashtag + "</a>";
}

// https://www.sitepoint.com/url-parameters-jquery/
function getUrlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
