
$.getJSON("data/table-tweet-important.json", function(json) {
  $.each(json, function(i, elem){
    var row =  $('<tr></tr>');

    row.append('<td>' + elem.ranking + '</td>');

    var cell = $('<td></td>');
    var media = $('<div class="media"></div>');
    var medialeft = $('<div class="media-left"></div>');
    var mediabody = $('<div class="media-body"></div>');
    //medialeft.append('<img class="media-object" src="https://twitter.com/' + elem.autor + '/profile_image?size=bigger">');
    mediabody.append('<h4 class="media-heading">' + elem.autorname + ' <small>@' + elem.autor + '</small></h4>');
    var text = elem.text;
    $.each(elem.hashtags, function(i, normalizedhashtag) {
      var hregex = new RegExp('#' + normalizedhashtag, 'i');
      var hit = text.match(hregex);
      if (hit != null) {
        $.each(hit, function(i, hashtag){
          text = text.replace(hashtag, linkHashtagInTweet(hashtag.replace('#', ''), normalizedhashtag));
        });
      }

    });
    mediabody.append(text);

    media.append(medialeft);
    media.append(mediabody);
    cell.append(media);
    row.append(cell);

    row.append('<td>' + new Date(elem.zeitpunkt).toISOString() + '</td>');
    row.append('<td>' + elem.favourites + '</td>');
    row.append('<td>' + elem.retweets + '</td>');

    $('#table-tweet-important tbody').append(row);

  });

  $('#table-tweet-important').DataTable();
      $(document).ready(function() {
  });

});
