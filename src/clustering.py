import query
import psycopg2
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.cluster import KMeans
from scipy import stats
from scipy import cluster

# Programm einfach ausführen, um Analyse auszuführen und alle drei Graphen (Elbow Graph und die zwei Scatter Plots mit Clustering) zu zeigen

# allhashclusters => Dictionary mit alle Hashtags als Keys und eine 2er Liste, die zeigt, in welche Cluster Hashtag vorkommt; 1. Zahl entspricht impactclusters und 2. Zahl entspricht cndd8clusters
# impactclusters => Clusters nach Häufigkeit und RTs+Favs; Dictionary mit Clustername als Keys
# cndd8clusters => Clusters nach Verwendung von Trump und Hillary; Dictionary mit Clustername
# impactgraph => Scatter Plot mit Clusters nach Häufigkeit und RTs+Favs
# cndd8graph => Scatter Plot mit Clusters nach Verwendung von Trump and Hillary

class hashanalysis():
	def __init__(self):
		#All the analysis is done when a hashanalysis object is created
		self.dbquery = query.database()
		self.hashrtfav = self.dbquery.chashrtfav()
		self.trumptags = self.dbquery.ctrumphashtags()
		self.clinttags = self.dbquery.cclintonhashtags()
		self.dbquery.close()

		self.trumptags.sort(key=lambda x: x[0])
		self.clinttags.sort(key=lambda x: x[0])

		self.thash = 0
		self.chash = 0

		for i in range(0, len(self.hashrtfav)):
			self.hashrtfav[i].append(self.hashrtfav[i][4]/self.hashrtfav[i][1]) # durchscnittliche summe von RTs und Favs
			if (self.thash < len(self.trumptags) and self.hashrtfav[i][0] == self.trumptags[self.thash][0]):
				self.hashrtfav[i].append(self.trumptags[self.thash][1])
				self.thash+=1
			else:
				self.hashrtfav[i].append(0)
			if (self.chash < len(self.clinttags) and self.hashrtfav[i][0] == self.clinttags[self.chash][0]):
				self.hashrtfav[i].append(self.clinttags[self.chash][1])
				self.chash+=1
			else:
				self.hashrtfav[i].append(0)

		self.total_hashes = sum([elem[1] for elem in self.hashrtfav])
		self.total_rts = sum([elem[2] for elem in self.hashrtfav])
		self.total_favs = sum([elem[3] for elem in self.hashrtfav])
		self.total_rts_favs = sum([elem[4] for elem in self.hashrtfav])
		self.total_dschnitt = sum([elem[5] for elem in self.hashrtfav])

		### K-Means: Häufigkeit% + RT+Fav%
		self.hfgarray = np.array([float(item[1]) for item in self.hashrtfav]) # Häufigkeit
		self.dstarray = np.array([float(item[4]) for item in self.hashrtfav]) # RT+Fav
		self.stacked01 = np.column_stack((self.hfgarray,self.dstarray))
		self.kmeans01 = KMeans(n_clusters=4, random_state=0, precompute_distances=True).fit(self.stacked01)

		self.grp0 = []
		self.grp1 = []
		self.grp2 = []
		self.grp3 = []

		for i in range(0, self.stacked01.shape[0]):
			if self.kmeans01.labels_[i] == 0:
				self.hashrtfav[i].append(0)
				self.grp0.append(self.hashrtfav[i][0])
			elif self.kmeans01.labels_[i] == 1:
				self.hashrtfav[i].append(1)
				self.grp1.append(self.hashrtfav[i][0])
			elif self.kmeans01.labels_[i] == 2:
				self.hashrtfav[i].append(2)
				self.grp2.append(self.hashrtfav[i][0])
			elif self.kmeans01.labels_[i] == 3:
				self.hashrtfav[i].append(3)
				self.grp3.append(self.hashrtfav[i][0])

		### K-Means: Trump-Clinton
		self.djtarray = np.array([float((item[6]/item[1])*100) for item in self.hashrtfav]) # Trump%
		self.hrcarray = np.array([float((item[7]/item[1])*100) for item in self.hashrtfav]) # Clinton%
		self.stacked02 = np.column_stack((self.djtarray,self.hrcarray))
		self.kmeans02 = KMeans(n_clusters=3, random_state=0, precompute_distances=True).fit(self.stacked02)

		self.grp4 = []
		self.grp5 = []
		self.grp6 = []

		for i in range(0, self.stacked02.shape[0]):
			if self.kmeans02.labels_[i] == 0:
				self.hashrtfav[i].append(0)
				self.grp4.append(self.hashrtfav[i][0])
			elif self.kmeans02.labels_[i] == 1:
				self.hashrtfav[i].append(1)
				self.grp5.append(self.hashrtfav[i][0])
			elif self.kmeans02.labels_[i] == 2:
				self.hashrtfav[i].append(2)
				self.grp6.append(self.hashrtfav[i][0])

	def allhashclusters(self):
		# gibt Dictionary mit Hashtag als Key und Liste von deren Clusters zurück
		# impactclusters: 0 = Rarely Used Hashtags, grün; 1 = Very Influential Hashtags, rot; 2 = Influential Hashtags, blau; 3 = Notable Hashtags, Magenta
		#cndd8clusters: 0 = Trump Hashtags, rot; 1 = Clinton Hashtags, blau; 2 = Neutral Hashtags, magenta
		hashdict = {item[0]: [item[8], item[9]] for item in self.hashrtfav}
		return hashdict

	def impactclusters(self):
		# gibt Dictionary mit Name des Clusters als Key und Liste von Mitglieder den Clusters zurück
		impdict = {
            "Rarely Used Hashtags" : self.grp0,
            "Very Influential Hashtags": self.grp1,
            "Influential Hashtags": self.grp2,
            "Notable Hashtags" : self.grp3,
        }
		return impdict

	def cndd8clusters(self):
		# gibt Dictionary mit Name des Clusters als Key und Liste von Mitglieder den Clusters zurück
		djthrcdict = {"Trump Hashtags": self.grp4, "Clinton Hashtags": self.grp5, "Neutral Hashtags": self.grp6}
		return djthrcdict

	def impactgraph(self):
		# zeigt unser Clusters für Häufigkeit und RTs+Favs
		for i in range(0, self.stacked01.shape[0]):
			if self.kmeans01.labels_[i] == 0:
				c1 = plt.scatter(self.stacked01[i,0],self.stacked01[i,1],c='g',marker='*')
			elif self.kmeans01.labels_[i] == 1:
				c2 = plt.scatter(self.stacked01[i,0],self.stacked01[i,1],c='r',marker='*')
			elif self.kmeans01.labels_[i] == 2:
				c3 = plt.scatter(self.stacked01[i,0],self.stacked01[i,1],c='b', marker='*')
			elif self.kmeans01.labels_[i] == 3:
				c3 = plt.scatter(self.stacked01[i,0],self.stacked01[i,1],c='m', marker='*')
		plt.xlabel("Number of Occurences of a Particular Hashtag")
		plt.ylabel("Total Retweets and Favourites of Tweets Containing a Particular Hashtag")
		plt.show()

	def cndd8graph(self):
		# zeigt unser "Clusters" für Trump und Clinton
		for i in range(0, self.stacked02.shape[0]):
			if self.kmeans02.labels_[i] == 0:
				c1 = plt.scatter(self.stacked02[i,0],self.stacked02[i,1],c='r',marker='*')
			elif self.kmeans02.labels_[i] == 1:
				c2 = plt.scatter(self.stacked02[i,0],self.stacked02[i,1],c='b',marker='*')
			elif self.kmeans02.labels_[i] == 2:
				c3 = plt.scatter(self.stacked02[i,0],self.stacked02[i,1],c='m', marker='*')
		plt.xlabel("Percentage of Tweets by Trump with a Particular Hashtag")
		plt.ylabel("Percentage of Tweets by Clinton with a Particular Hashtag")
		plt.show()

def main():
    # show all our analyses
	analyse = hashanalysis()
	cluster_array = [cluster.vq.kmeans(analyse.stacked01, i) for i in range(1,10)]
	plt.plot([snd for (fst,snd) in cluster_array])
	plt.show()
#	plt.savefig('elbow.png')
	analyse.impactgraph()
	analyse.cndd8graph()

if __name__ == '__main__':
	main()
