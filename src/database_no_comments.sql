SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE TABLE hashtag (
    text character(280) NOT NULL
);

ALTER TABLE hashtag OWNER TO student;

ALTER TABLE ONLY hashtag
    ADD CONSTRAINT "Text" PRIMARY KEY (text);

CREATE TABLE tweet (
    id integer NOT NULL,
    zeitpunkt integer NOT NULL,
    retweets integer NOT NULL,
    favourites integer NOT NULL,
    text character(280) NOT NULL,
    autor character(15) NOT NULL
);

ALTER TABLE tweet OWNER TO student;

ALTER TABLE ONLY tweet
    ADD CONSTRAINT "ID" PRIMARY KEY (id);

CREATE TABLE enthaelt (
    anzahl integer,
    text character(280) NOT NULL,
    id integer NOT NULL
);

ALTER TABLE enthaelt OWNER TO student;

ALTER TABLE ONLY enthaelt
    ADD CONSTRAINT "ID & Text" PRIMARY KEY (text, id);

ALTER TABLE ONLY enthaelt
    ADD CONSTRAINT "ID aus Tweet" FOREIGN KEY (id) REFERENCES tweet(id);

ALTER TABLE ONLY enthaelt
    ADD CONSTRAINT "Text aus Hashtag" FOREIGN KEY (text) REFERENCES hashtag(text);

CREATE TABLE trittaufmit (
    texta character(280) NOT NULL,
    textb character(280) NOT NULL,
    id integer NOT NULL
);

ALTER TABLE trittaufmit OWNER TO student;


ALTER TABLE ONLY trittaufmit
    ADD CONSTRAINT "superkey trittaufmit" PRIMARY KEY (texta, textb, id);

ALTER TABLE ONLY trittaufmit
    ADD CONSTRAINT "HashtagA" FOREIGN KEY (texta) REFERENCES hashtag(text);

ALTER TABLE ONLY trittaufmit
    ADD CONSTRAINT "HashtagB" FOREIGN KEY (textb) REFERENCES hashtag(text);


ALTER TABLE ONLY trittaufmit
    ADD CONSTRAINT "id von tweet" FOREIGN KEY (id) REFERENCES tweet(id);