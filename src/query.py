#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Quellen: http://initd.org/psycopg/docs/usage.html

import psycopg2
# Auf ubuntu heisst das zu installierende Paket python-psycopg2

#Alle SQL Abfragen laufen gleich ab: Verbindung mit der Datenbank aufbauen, SQL Abfrage auf die Datenbank 
#und geliefertes Ergebnis "säubern" und für weitern Gebrauch anpassen

#ctwmithash => Liste von allen Tweet IDs mit bestimmten Hashtags
#cwichtig => abwärts geordnete Liste von Tweets mit den meisten Retweets + Favourites
#cmeist => abwärts geordnete Liste von den meist benutzten Hashtags
#chashrtfav => lexikographisch geordnete Liste von hashtags mit Anzahl deren Verwendung und Summe von alle RTs und Favs
#chashprotag => liste mit vorkommen eines hashtags pro tag, sortiert nach datum und hashtag
#chashprotagalle => liste mit vorkommen von hashtags pro tag, sortiert nach darum
#chashzusammen => abwärts geordnete Liste von den meist miteinander verwendeten Hashtags in allen Tweets
#cfilahash => alle verwendeten Hashtags mit ihrem ersten und letzten Vorkommen in den Daten
#ctrumphashtags => alle von Trump verwendeten Hashtags mit Anzahl
#cclintonhashtags => alle von Hillary verwendeten Hashtags mit Anzahl



class database():
    """docstring for database."""
    def __init__(self):
        # connection
        self.conn = psycopg2.connect(host='agdbs-edu01.imp.fu-berlin.de' ,port='5432' ,dbname='Election_Yannis_Ren_Raphael' ,user='student' ,password='password') #damit können wir eine Verbindung mit unserer Datenbank erstellen
        self.cur = self.conn.cursor()

    def close(self):
        # commit changes and close connection
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def ctwmithash(self, limit=None):
        #alle tweets mit hashtagX
        self.cur.execute("""
            SELECT TW.id, H.text
            FROM tweet TW JOIN enthaelt E ON TW.id = E.id JOIN hashtag H ON H.text = E.text
            ORDER BY TW.id
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        twmithash = self.cur.fetchall()

        ctwmithash = [list(elem) for elem in twmithash]
        for i in range(0, len(ctwmithash), 1):
            ctwmithash[i][1] = ctwmithash[i][1].strip()
        return ctwmithash

    def cwichtig(self, limit=None):
        # wichtigste tweets
        self.cur.execute("""
            SELECT *
            FROM tweet
            ORDER BY retweets+favourites DESC
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        wichtig = self.cur.fetchall()

        cwichtig = [list(elem) for elem in wichtig]
        for i in range(0, len(cwichtig), 1):
            cwichtig[i][4] = cwichtig[i][4].strip()
            cwichtig[i][5] = cwichtig[i][5].strip()
        return cwichtig

    def cmeist(self, limit=None):
        # meist verwendete hashtags
        self.cur.execute("""
            SELECT text, SUM (anzahl) as sumanzahl
            FROM enthaelt
            GROUP BY text
            ORDER BY sumanzahl DESC
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        meist = self.cur.fetchall()

        cmeist = [list(elem) for elem in meist]
        for i in range(0, len(cmeist), 1):
            cmeist[i][0] = cmeist[i][0].strip()
        return cmeist

    def chashrtfav(self, limit=None):
        #alle hashtags, Anzahl deren Verwendung, und Summe aller RTs und Favs von Tweets mit diesem hashtag
        self.cur.execute("""
            SELECT E.text, SUM (anzahl) as sumanzahl, SUM (retweets) as rts, SUM (favourites) as favs
            FROM enthaelt E JOIN tweet T ON E.id = T.id
            GROUP BY E.text
            ORDER BY E.text
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        hashusertfav = self.cur.fetchall()

        chashes = [list(elem) for elem in hashusertfav]
        for i in range(0, len(chashes), 1):
            chashes[i][0] = chashes[i][0].strip()
            chashes[i].append(chashes[i][2]+chashes[i][3])
        return chashes

    def chashprotag(self, limit=None):
        #vorkommen eins bestimmten hashtags pro tag
        self.cur.execute("""
            SELECT date(to_timestamp(zeitpunkt)) as convzeitpunkt, H.text, SUM (anzahl) as sumanzahl
            FROM enthaelt E JOIN hashtag H ON H.text = E.text JOIN tweet T ON T.id = E.id
            GROUP BY convzeitpunkt, H.text
            ORDER BY convzeitpunkt, H.text
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        hashprotag = self.cur.fetchall()

        chashprotag = [list(elem) for elem in hashprotag]
        for i in range(0, len(chashprotag), 1):
            chashprotag[i][1] = chashprotag[i][1].strip()
            chashprotag[i][0] = chashprotag[i][0].strftime("%Y-%m-%d")
        return chashprotag

    def chashprotagalle(self, limit=None):
        #vorkommen von hashtags pro tag
        self.cur.execute("""
            SELECT date(to_timestamp(zeitpunkt)) as convzeitpunkt, SUM (anzahl) as sumanzahl
            FROM enthaelt E JOIN tweet T ON T.id = E.id
            GROUP BY convzeitpunkt
            ORDER BY convzeitpunkt
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        hashprotagalle = self.cur.fetchall()

        chashprotagalle = [list(elem) for elem in hashprotagalle]
        for i in range(0, len(chashprotagalle), 1):
            chashprotagalle[i][0] = chashprotagalle[i][0].strftime("%Y-%m-%d")
        return chashprotagalle

    def chashzusammen(self, limit=None):
        #zusammen auftretende hashtags
        self.cur.execute("""
            SELECT texta, textb, SUM(1) as sumanzahl
            FROM trittaufmit
            WHERE texta >= textb
            GROUP BY texta, textb
            ORDER BY sumanzahl DESC
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        hashzusammen = self.cur.fetchall()

        chashzusammen = [list(elem) for elem in hashzusammen]
        for i in range(0, len(chashzusammen), 1):
            chashzusammen[i][0] = chashzusammen[i][0].strip()
            chashzusammen[i][1] = chashzusammen[i][1].strip()
        return chashzusammen

    def cfilahash(self, limit=None):
        #ersterundletztertag jedes hashtags
        self.cur.execute("""
            SELECT H.text, zeitpunkt
            FROM hashtag H JOIN enthaelt E ON H.text = E.text JOIN tweet TW ON E.id = TW.id
            ORDER BY H.text, TW.zeitpunkt
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        filahash = self.cur.fetchall()

        cfilahash = [list(elem) for elem in filahash]
        for i in range(0, len(cfilahash), 1):
            cfilahash[i][0] = cfilahash[i][0].strip()
            #cfilahash[i][1] = cfilahash[i][1].strftime("%Y-%m-%d")
        
        #Alle Daten eines Hashtags werden ausgegeben mit dem Hashtag und alle "mittleren Daten"
        #werden aus der Liste entfernt, am Ende ist es eine Liste von Hashtags mit ihrem ersten und letzten Vorkommen
        
        cfilahash2=[]
        iterlist=[]
        j=0
        while j < len(cfilahash):
            iterlist.append(cfilahash[j])
            hashtag = cfilahash[j][0]
            while (j<len(cfilahash)) and ((cfilahash[j][0]) == (hashtag)):
                j=j+1
            (iterlist[0]).append(cfilahash[j-1][1])
            cfilahash2.append(iterlist[0])
            iterlist=[]
        cfilahash = cfilahash2

        return cfilahash

    def ctrumphashtags(self, limit=None):
        #hashtags von trump
        self.cur.execute("""
            SELECT E.text, SUM (anzahl) as sumanzahl
            FROM enthaelt E JOIN tweet TW ON tw.id = e.id
            WHERE autor = 'realDonaldTrump'
            GROUP BY E.text
            ORDER BY sumanzahl DESC
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        trumphashtags = self.cur.fetchall()

        ctrumphashtags = [list(elem) for elem in trumphashtags]
        for i in range(0, len(ctrumphashtags), 1):
            ctrumphashtags[i][0] = ctrumphashtags[i][0].strip()
        return ctrumphashtags

    def cclintonhashtags(self, limit=None):
        #hashtags von hillary
        self.cur.execute("""
            SELECT E.text, SUM (anzahl) as sumanzahl
            FROM enthaelt E JOIN tweet TW ON tw.id = e.id
            WHERE autor = 'HillaryClinton'
            GROUP BY E.text
            ORDER BY sumanzahl DESC
        """ + ("LIMIT " + str(limit) if limit != None  else ""))
        clintonhashtags = self.cur.fetchall()

        cclintonhashtags = [list(elem) for elem in clintonhashtags]
        for i in range(0, len(cclintonhashtags), 1):
            cclintonhashtags[i][0] = cclintonhashtags[i][0].strip()
        return cclintonhashtags


def main():
    # use case:
    db = database()
    result = db.cclintonhashtags()
    print(result)
    result = db.cclintonhashtags(10)
    print(result)
    db.close()

if __name__ == '__main__':
    main()
