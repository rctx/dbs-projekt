Wir haben verschiedene mögliche Dimensionen gewählt und dann versucht, welche am Ende sinnvoll werden. Wir haben die Kombinationen einfach getestet und angeschaut, ob es eine nutzliche Clustering existieren kann.

Zur Analyse unsere Daten haben wir ein Programm in Python erstellt. Dieses Programm nutzt verschiedene Pakete, die die Analyse und eventuelle graphische Darstellung vereinfachen.

* matplotlib.pyplot - Für die Erzeugung von Graphen
* numpy - Für die Erzeugung von Arrays, die notwendig für die Clusteranalyse sind
* sklearn - Für die eignetliche Datenanalyse, u.a. k-means Clustering
* scipy - Für unser Ellenbogengraph

Ein großes Problem unseren Datensatzes ist, dass ungefähr zwei drittel aller Hashtags nur einmal verwendet werden. D.h., wenn wir irgendwelche Analyse durchführt, gibt es in der Regel eine (oder auch mehrere) Clusters, die nur aus diese Hashtags bestehen, oder auch, wenn wir zum Beispiel erstes und letztes Vorkommen eines Hashtags in UNIX Time als Dimensionen auswählen, eine klare Gerade von solche Hashtags. Wir haben überlegt, ob es sinnvoll ist, die Hashtags erstmals zu analysieren, ohne diese Hashtags, aber dann wird unser Datensatz noch kleiner. Am Ende haben wir unsere Dimensionen so gewählt, dass diese große Cluster von einmalige Hashtags sinnvoll interpretierbar ist.

Am Ende haben wir als Dimensionen die Anzahl der Verwendung eines bestimmten Hashtags und die gesamte Anzahl von Retweets und Favourites aller Tweets, die ein bestimmtes Hashtag enthält, gewählt. Wir haben einen Ellenbogengraph erzeugt, damit wir bestimmen können, was eine sinnvolle k wird. Der Graph ist nach jeder Ausführung anders, aber nach mehrere Graphen haben wir uns für k = 4 entschieden.

Wir können unsere vier Clusters so beschreiben:

* "Very Influential Hashtags" sind Hashtags, die meist verwendet werden und die meiste gesamte Retweets und Favourites hat. Diese Cluster besteht aus #makeamericagreatagain und #trump2016. Wir merken, dass diese zwei Hashtags weit entfernt von alle andere Hashtags im Graph sind.
* "Influential Hashtags" sind Hashtags, die auch relativ häufig verwendet werden und in Tweets mit viele Retweets und Favourites kommt. Diese Cluster besteht aus #americafirst, #debatenight, #imwithyou, und #maga.
* "Notable Hashtags" sind Hashtags, die vielleicht genauso oder fast so oft verwendet werden aber deren Tweets nicht so viele Retweets und Favourites wie die letzt Cliuster hat. Diese Cluster besteht aus #crookedhillary, #debates2016, #demsinphilly, #rncincle, #supertuesday, #trumppence16, #trumptrain, und #votetrump.
* "Uncommon Hashtags" sind Hashtags die vielleicht nur einmal verwendet werden, oder nicht so häufig benutzt, oder in Tweets mit weniger Retweets und Favourites sind. Das ist unsere größte Cluster.

Wir haben auch eine Clusteranalyse ausgeführt, wobei die Dimensionen der Anteil der Verwendung eines Hashtags, der von Trump ist, und der Anteil, der von Clinton ist, sind. Da die Zahlen streng korreliert sind, ist das nicht unsere Hauptanalyse, aber wir haben das durchgeführt eher zur Interesse, und eventuell für unsere Visualisierung.