--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.1

-- Started on 2017-05-31 16:00:43

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 182 (class 1259 OID 18067)
-- Name: hashtag; Type: TABLE; Schema: public; Owner: student
--

CREATE TABLE hashtag (
    text character(280) NOT NULL
);


ALTER TABLE hashtag OWNER TO student;


--
-- TOC entry 2005 (class 2606 OID 18279)
-- Name: Text; Type: CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY hashtag
    ADD CONSTRAINT "Text" PRIMARY KEY (text);


--
-- TOC entry 181 (class 1259 OID 18062)
-- Name: tweet; Type: TABLE; Schema: public; Owner: student
--

CREATE TABLE tweet (
    id integer NOT NULL,
    zeitpunkt integer NOT NULL,
    retweets integer NOT NULL,
    favourites integer NOT NULL,
    text character(280) NOT NULL,
    autor character(15) NOT NULL
);


ALTER TABLE tweet OWNER TO student;


--
-- TOC entry 2005 (class 2606 OID 18066)
-- Name: ID; Type: CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY tweet
    ADD CONSTRAINT "ID" PRIMARY KEY (id);

	
	

--
-- TOC entry 183 (class 1259 OID 18072)
-- Name: enthaelt; Type: TABLE; Schema: public; Owner: student
--

CREATE TABLE enthaelt (
    anzahl integer,
    text character(280) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE enthaelt OWNER TO student;


--
-- TOC entry 2005 (class 2606 OID 18304)
-- Name: ID & Text; Type: CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY enthaelt
    ADD CONSTRAINT "ID & Text" PRIMARY KEY (text, id);


--
-- TOC entry 2006 (class 2606 OID 18084)
-- Name: ID aus Tweet; Type: FK CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY enthaelt
    ADD CONSTRAINT "ID aus Tweet" FOREIGN KEY (id) REFERENCES tweet(id);


--
-- TOC entry 2007 (class 2606 OID 18305)
-- Name: Text aus Hashtag; Type: FK CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY enthaelt
    ADD CONSTRAINT "Text aus Hashtag" FOREIGN KEY (text) REFERENCES hashtag(text);


--
-- TOC entry 184 (class 1259 OID 18077)
-- Name: trittaufmit; Type: TABLE; Schema: public; Owner: student
--

CREATE TABLE trittaufmit (
    texta character(280) NOT NULL,
    textb character(280) NOT NULL,
    id integer NOT NULL
);


ALTER TABLE trittaufmit OWNER TO student;



--
-- TOC entry 2005 (class 2606 OID 18326)
-- Name: superkey trittaufmit; Type: CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY trittaufmit
    ADD CONSTRAINT "superkey trittaufmit" PRIMARY KEY (texta, textb, id);


--
-- TOC entry 2006 (class 2606 OID 18285)
-- Name: HashtagA; Type: FK CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY trittaufmit
    ADD CONSTRAINT "HashtagA" FOREIGN KEY (texta) REFERENCES hashtag(text);


--
-- TOC entry 2007 (class 2606 OID 18290)
-- Name: HashtagB; Type: FK CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY trittaufmit
    ADD CONSTRAINT "HashtagB" FOREIGN KEY (textb) REFERENCES hashtag(text);


--
-- TOC entry 2008 (class 2606 OID 18320)
-- Name: id von tweet; Type: FK CONSTRAINT; Schema: public; Owner: student
--

ALTER TABLE ONLY trittaufmit
    ADD CONSTRAINT "id von tweet" FOREIGN KEY (id) REFERENCES tweet(id);

	
-- Completed on 2017-05-31 16:00:43

--
-- PostgreSQL database dump complete
--

