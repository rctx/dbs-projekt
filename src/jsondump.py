#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import query, json, datetime, copy, math, clustering
folder = "../html/data/"

class factory():
    """docstring for dump."""
    def __init__(self):
        # benutze clustering modul
        self.db = query.database()
        self.chashprotagalle =  self.db.chashprotagalle()
        self.cwichtig = self.db.cwichtig()
        self.cmeist = self.db.cmeist()
        self.chashzusammen = self.db.chashzusammen()
        self.ctwmithash = self.db.ctwmithash()
        self.chashprotag = self.db.chashprotag()
        self.chashrtfav = self.db.chashrtfav()
        self.handle = {"realDonaldTrump" : "Donald J. Trump", "HillaryClinton" : "Hillary Clinton"}

        # erzuge dictionaries für verschiedene abbildungen zwischen hashtag und tweet
        self.tweet2hash = {}
        self.hash2tweet = {}
        self.hash2hash = {}
        for (id, zeitpunkt, retweets, favourites, text, autor) in self.cwichtig:
            self.tweet2hash[id] = []
        for (text, anzahl) in self.cmeist:
            self.hash2tweet[text] = []
            self.hash2hash[text] = []
        for (tweetid, hashtag) in self.ctwmithash:
            if hashtag not in self.tweet2hash[tweetid]:
                self.tweet2hash[tweetid].append(hashtag)
            if tweetid not in self.hash2tweet[hashtag]:
                self.hash2tweet[hashtag].append(tweetid)
        for (texta, textb, summe) in self.chashzusammen:
            if textb not in self.hash2hash[texta]:
                self.hash2hash[texta].append(textb)
            if texta not in self.hash2hash[textb]:
                self.hash2hash[textb].append(texta)

        self.hashanalysis = clustering.hashanalysis()

        self.clusterColor = {
            0 : "#008000",  # grün
            1 : "#ff0303",  # rot
            2 : "#0000ff",  # blau
            3 : "#bf00bf",  # magenta
        }

        self.clusterColorEdge = {
            0 : "#5a805a",  # grün
            1 : "#ffb3b3",  # rot
            2 : "#b3b3ff",  # blau
            3 : "#bf86bf",  # magenta
        }

        self.clusterName = {
            0 : "Rarely Used Hashtags",
            1 : "Very Influential Hashtags",
            2 : "Influential Hashtags",
            3 : "Notable Hashtags",
        }

    # speicher das json
    def dump(self, data, filename):
        f = open(folder + filename, "w")
        output = json.dumps(data, indent=1, sort_keys=True)
        f.write(output)
        f.close

    # gebe Abbildung von tweet auf hash zurück
    # (alle hashtags in einem tweet)
    def maptweet2hash(self):
        return self.tweet2hash

    # gebe Abbildung von hash auf tweet zurück
    # (alle tweets die hash enthalten)
    def maphash2tweet(self):
        return self.hash2tweet

    # gebe Abbildung von hash auf hash zurück
    # (Hashtags, die miteinander auftreten)
    def maphash2hash(self):
        return self.hash2hash

    # Funktion zur Erzeugung des eines dictionaries
    # (für jeden hashtag ein eigener Datenatz wie in barchartHashtagAll())
    def barchartHashtag(self):
        data = copy.deepcopy(self.chashprotag)
        for i in range(len(data)):
            data[i][0] = data[i][0].replace("-", "/")

        mindate = min(data, key=lambda x : x[0])[0]
        mindate_list = mindate.split("/")
        mindate = mindate_list[0] + "/" + mindate_list[1] + "/01"

        maxdate = max(data, key=lambda x : x[0])[0]
        maxdate_list = maxdate.split("/")
        maxdate = mindate_list[0] + "/" + "%.02d" % (int(maxdate_list[1]) + 1) + "/01"

        maindict = {}
        for (text, anzahl) in self.cmeist:
            maindict[text] = {"data" : []}
            maindict[text]["min"] = mindate
            maindict[text]["max"] = maxdate
        for (zeitpunkt, text, anzahl) in data:
            maindict[text]["data"].append([zeitpunkt, anzahl])

        return maindict

    # Funktion zur Erzeugung eines dictionaries vom Auftreten der Hashtags
    # enthällt Liste mit Wert für jedes Datum, sowie Start und End-Datum
    def barchartHashtagAll(self):
        data = copy.deepcopy(self.chashprotagalle)
        for i in range(len(data)):
            data[i][0] = data[i][0].replace("-", "/")
        mindate = min(data, key=lambda x : x[0])[0]
        maxdate = max(data, key=lambda x : x[0])[0]
        dict = {}

        mindate_list = mindate.split("/")
        mindate = mindate_list[0] + "/" + mindate_list[1] + "/01"
        dict["min"] = mindate

        maxdate_list = maxdate.split("/")
        maxdate = mindate_list[0] + "/" + "%.02d" % (int(maxdate_list[1]) + 1) + "/01"
        dict["max"] = maxdate

        dict["data"] = data
        return dict

    # erzeuge Quickfacts für die Hauptseite
    def listQuickfacts(self):
        tweetAnzahl = len(self.cwichtig)
        hashtagAnzahl = len(self.cmeist)
        #https://stackoverflow.com/questions/8419564/difference-between-two-dates
        mindate = min(self.chashprotagalle, key=lambda x : x[0])[0]
        maxdate = max(self.chashprotagalle, key=lambda x : x[0])[0]
        d1 = datetime.datetime.strptime(mindate, "%Y-%m-%d")
        d2 = datetime.datetime.strptime(maxdate, "%Y-%m-%d")
        tage = (d2 - d1).days
        kandidaten = 2
        return [str(tweetAnzahl) + " tweets", str(hashtagAnzahl) + " hashtags",
                str(tage) + " days", str(kandidaten) + " candidates"]

    # erzeuge Quickfacts für die Seite mit dem einzelnen Hahstag
    def listQuickfactsHashtag(self):
        dict = {}
        for (text, anzahl) in self.cmeist:
            dict[text] = {}
            dict[text]["tweets"] = len(self.maphash2tweet()[text])
            dict[text]["hashtags"] = sorted(self.maphash2hash()[text])
            clusternr = self.hashanalysis.allhashclusters()[text][0]
            dict[text]["clustername"] = self.clusterName[clusternr]
            dict[text]["clustercolor"] = self.clusterColor[clusternr]

        return dict

    # erzeuge Daten für die Hashtagtabelle mit einzelnen Hashtags
    def hashtagAll(self):
        ranking = 1
        count = 1
        last = -1
        list = []
        for (hashtag, anzahl) in self.cmeist:
            if anzahl != last:
                ranking = count
            list.append({"ranking" : ranking, "hashtag" : hashtag, "count" : anzahl})
            count += 1
            last = anzahl
        return list

    # erzeuge Daten für die Hashtagtabelle mit Paaren
    def hashtagPairsAll(self):
        ranking = 1
        count = 1
        last = -1
        list = []
        for (texta, textb, anzahl) in self.chashzusammen:
            if anzahl != last:
                ranking = count
            list.append({"ranking" : ranking, "texta" : texta, "textb" : textb, "count" : anzahl})
            count += 1
            last = anzahl
        return list

    # erzeuge Daten für die Tweettabelle
    def tableTweetAll(self):
        result = {}
        ranking = 1
        for (id, zeitpunkt, retweets, favourites, text, autor) in self.cwichtig:
            result[id] = {
                "zeitpunkt" : zeitpunkt * 1000,
                "favourites" : favourites,
                "retweets" : retweets,
                "text" : text,
                "autor" : autor,
                "autorname" : self.handle[autor],
                "hashtags" : self.maptweet2hash()[id],
                "ranking" : ranking
            }
            ranking += 1
        return result

    # erzeuge Daten für die Tweettabelle (mit Begrenzung)
    def tableTweetImportant(self):
        result = {}
        ranking = 1
        for (id, zeitpunkt, retweets, favourites, text, autor) in self.cwichtig[0:100]:
            result[id] = {
                "zeitpunkt" : zeitpunkt * 1000,
                "favourites" : favourites,
                "retweets" : retweets,
                "text" : text,
                "autor" : autor,
                "autorname" : self.handle[autor],
                "hashtags" : self.maptweet2hash()[id],
                "ranking" : ranking
            }
            ranking += 1
        return result

    # erzeuge Tweettabellen für die Seiten der einzelnen Hashtags
    def tableTweetHashtagAll(self):
        allTweets = self.tableTweetAll()
        result = {}
        for (text, anzahl) in self.cmeist:
            result[text] = []
            for tweetid in self.maphash2tweet()[text]:
                result[text].append(allTweets[tweetid])
        return result

    # erzeuge die Daten für die positionierung der Hashtags im Netzwerk
    # mit den Angaben der Kanten
    def networkHashtag(self):
        #
        ### 1. Schritt: liste von Bäumen erzeugen
        #               Wir gehen naiv davon aus, dass alle Zusammenhangskomponenten
        #               im Hashtagnetzwerk Bäume sind. Das ist zwar nicht der Fall,
        #               aber dieses Vorgehen hilft trotzdem dabei, einen
        #               gut lesbaren Graphaen zu erzeugen.
        #

        hashtag_nodes_all = []
        for (text, _) in self.cmeist:
            hashtag_nodes_all.append(text)
        hashtag_nodes_important = copy.copy(hashtag_nodes_all)
        # sortiere zunächst alphabetisch
        hashtag_nodes_all = sorted(hashtag_nodes_all)
        #sortiere (stabil) knoten nach grad, also hashtags nach anzahl von nachbarhashtags
        hashtag_nodes_all = sorted(hashtag_nodes_all, key=lambda x : len(self.maphash2hash()[x]), reverse=True)
        hashtag_nodes_left = copy.copy(hashtag_nodes_all)

        # abgweandete Breitensuche, zur Erstellung von Unterbäumen im Graphen:
        node_trees = []
        stack = []
        while(len(hashtag_nodes_left) > 0):
            if (len(stack) == 0):
                # neuer Baum mit dem nächsten noch vorhandnen Knoten höchsten
                # Grades als Wurzel
                root = hashtag_nodes_left.pop(0) # Hashtag ist nun vergeben
                tree = {root : []}
                node_trees.append(tree)
                stack.append(tree)

            subtree = stack.pop()
            root = list(subtree.keys())[0]
            leaves = subtree[root] # noch leere Liste
            potential_leaves = self.maphash2hash()[root] # alle adjaszenten knoten

            potential_leaves = sorted(potential_leaves)
            potential_leaves = sorted(potential_leaves, key=lambda x : len(self.maphash2hash()[x]), reverse=True)

            l = len(potential_leaves)
            p1 = potential_leaves[:(l//4)]
            p2 = potential_leaves[(l//4):]
            #print(p1)
            l1 = len(p1)
            l2 = len(p2)
            potential_leaves_2 = []
            for x in p1:
                potential_leaves_2.append(x)
                potential_leaves_2.extend(p2[:(l2//l1)])
                p2 = p2[(l2//l1):]
            potential_leaves_2.extend(p2)
            a = True
            b = True
            for x in potential_leaves:
                a = a and (x in potential_leaves_2)
            for x in potential_leaves_2:
                b = b and (x in potential_leaves)
            assert(a and b)
            potential_leaves = potential_leaves_2 [:]

            for leaf in potential_leaves:
                # Hahstags bzw. Blätter nur einfügen, wenn noch nicht vergeben
                if leaf in hashtag_nodes_left:
                    new_root = leaf
                    new_subtree = {new_root : []}
                    hashtag_nodes_left.remove(new_root) # Hashtag ist nun vergeben
                    leaves.append(new_subtree)
                    stack.append(new_subtree)

        nodes = []

        tree_count = 0
        little_tree_count = 0
        lonelyhashtag_count = 0
        for tree in node_trees:
            tree_count += 1
            root =  list(tree.keys())[0]
            leaves = tree[root]
            if leaves != []:
                little_tree_count +=1
            else:
                lonelyhashtag_count += 1

        #
        ### 2. Schritt, hashtags so positieren, wie wenn das Netzwerk nur aus
        #               Bäumen bestehen würde. Die Bäume sind natürlich keinen
        #               echten Bäume, sondern nur voneinander getrennte
        #               Zusammenhangskomponenten. Die Kanten, die von der Wurzel
        #               wie Strahlen ausgehen. Entsprechen den gedachten Kanten
        #               des naiv angenommmenen Baumes.
        #

        x_offset = 0 # x-Verschiebung eines Baumes
        y_offset = 0 # y-Verschiebung eines Baumes
        max_x = 0
        min_x = 0
        max_y = 0
        min_y = 0

        new_root = False
        stack = []
        # Anzahl an gesetzten Bäumen, nur der erste Baum ist kein little tree
        tree_placed_count = 0
        little_tree_placed_count = 0
        lonelyhashtag_placed_count = 0
        # rekursieves zeichnen der Bäume
        while (len(node_trees) > 0 ):
            if (len(stack) == 0):
                # starte mit nächstem Baum
                tree = node_trees.pop(0)
                root =  list(tree.keys())[0]
                leaves = tree[root]

                if tree_placed_count == 0: # erster großer Baum
                    # Polarkoordinaten
                    z = 0 # Abstand
                    arg = 0.25  # Argument
                    portion = 1 # erlaubter Anteil vom Kreis
                    step = 64
                    step_reduction_factor = 1.9 # Reduzierung des abstandes im nächsten Schritt
                    x_offset = 0
                    y_offset = 0
                    stack.append((tree, z, arg, portion, step, False))
                    tree_placed_count += 1
                    new_root = True

                else: # folgende Bäume
                    if leaves != []:
                        # Polarkoordinaten
                        z = 0 # Abstand
                        portion = 1 # erlaubter Anteil vom Kreis
                        step = 25
                        step_reduction_factor = 1.5
                        spread = max(max_x, max_y, abs(min_x), abs(min_y))
                        shift = 0.75 # verschiebung der Bäume im Uhrzeigersinn
                        # ausbreitung des ersten baumes:
                        base_z = math.sqrt(spread**2 + spread**2)

                        # kleine baume werden im kreis um den anderen herum platziert
                        h = 3 # Bäume im ersten Viertel
                        if (little_tree_placed_count <= h):
                            # Bäume im ersten viertel bekommen mehr Platz
                            fake_little_tree_count = h*4
                            arg = (1/fake_little_tree_count)*little_tree_placed_count + shift
                            x_offset = int(base_z * math.cos(arg*math.pi*2))
                            y_offset = int(base_z * math.sin(arg*math.pi*2))

                        else:
                            # die weiteren Bäume bekommen und brauchen weniger Platz
                            fake_little_tree_count = little_tree_count - h + (1/3) * (little_tree_count -h)
                            fake_little_tree_placed_count = (1/3) * little_tree_count + little_tree_placed_count -h
                            arg = (1/fake_little_tree_count)*fake_little_tree_placed_count + shift
                            x_offset = int(base_z * math.cos(arg*math.pi*2))
                            y_offset = int(base_z * math.sin(arg*math.pi*2))
                        stack.append((tree, z, arg, portion, step, False))
                        little_tree_placed_count += 1
                        tree_placed_count += 1
                        new_root = True

                    else:
                        # einsamer hashtag
                        # Polarkoordinaten
                        z = 0 # Abstand
                        portion = 1 # erlaubter Anteil vom Kreis
                        #step = 25
                        #step_reduction_factor = 1.5
                        spread = max(max_x, max_y, abs(min_x), abs(min_y))
                        # ausbreitung des ersten baumes:
                        base_z = math.sqrt(spread**2 + spread**2) + 60

                        # einsmae hashtags werden ganz außen drum herm platziert
                        arg = (1/lonelyhashtag_count)*lonelyhashtag_placed_count
                        x_offset = int(base_z * math.cos(arg*math.pi*2))
                        y_offset = int(base_z * math.sin(arg*math.pi*2))

                        stack.append((tree, z, arg, portion, step, True))
                        lonelyhashtag_placed_count += 1
                        new_root = False

            # zeichne neuen Unterbaum
            subtree, z, arg, portion, step, lonelyhashtag = stack.pop()
            root = list(subtree.keys())[0]
            leaves = subtree[root]

            if lonelyhashtag:
                # einzelne Hashtags
                nodes.append({
                    "id" : root,
                    "label" : "#" + root,
                    "x" : x_offset,
                    "y" : y_offset,
                    "size" : 1
                })

            else:
                # Hashtags in Zusammenhangskomponenten mit mehr als einem Hashtag
                x =  int(z * math.cos(arg*math.pi*2)) + x_offset
                y = int(z * math.sin(arg*math.pi*2)) + y_offset
                if tree_placed_count == 1:
                    max_x = max(max_x, x)
                    min_x = min(min_x, x)
                    max_y = max(max_y, y)
                    min_y = min(min_y, y)
                size = 1
                if (hashtag_nodes_important.index(root) < 30): # or hashtag_nodes_all.index(root) < 20):
                    size = 2
                if (hashtag_nodes_important.index(root) < 15): # or hashtag_nodes_all.index(root) < 15):
                    size = 3
                nodes.append({
                    "id" : root,
                    "label" : "#" + root,
                    "x" : x,
                    "y" : y,
                    "size" : size
                })

            for i in range(len(leaves)):
                new_leaf = leaves[i]
                new_arg = arg + (portion/len(leaves))*i
                new_z = z + step
                new_step = step/step_reduction_factor
                new_portion = portion/len(leaves)

                # manuelle Anpassungen
                if list(new_leaf.keys())[0] == "makeamericagreatagain":
                    new_portion = new_portion*4
                if root == "makeamericagreatagain":
                    new_arg = new_arg - 0.07

                if list(new_leaf.keys())[0] == "debatenight":
                    new_portion = new_portion*5
                if root == "debatenight":
                    new_arg = new_arg - 0.02

                if list(new_leaf.keys())[0] == "rncincle":
                    new_portion = new_portion*7


                stack.append((new_leaf, new_z, new_arg, new_portion, new_step, False))
            new_root = False

        #
        ### 3. Schritt: Erzeugen der Kanten
        #

        nodeid = 0
        edges = []
        for (texta, textb, anzahl) in self.chashzusammen:
            edges.append({
                "id" : "id" + str(nodeid),
                "source" : texta,
                "target" : textb
            })
            nodeid += 1
        return {"nodes" : nodes, "edges" : edges}

    # Erzeugung eines jsons, dass für jeden Cluster die Farbe, Kantenfarbe und
    # die enthaltenen Hashtags auflistet
    def networkHashtagCluster(self):

        hcluster = [1,2,3,0]
        for i in range(len(hcluster)):
            color = self.clusterColor[hcluster[i]]
            name = self.clusterName[hcluster[i]]
            hashtags = self.hashanalysis.impactclusters()[name]
            hcluster[i] = {
                "color" : self.clusterColor[hcluster[i]],
                "colorEdge" : self.clusterColorEdge[hcluster[i]],
                "name" : self.clusterName[hcluster[i]],
                "hashtags" : hashtags
            }
        return hcluster


def main():
    # erzeugung der json
    d = factory()
    data = d.barchartHashtag()
    d.dump(data, "barchart-hashtag.json")
    data = d.barchartHashtagAll()
    d.dump(data, "barchart-hashtag-all.json")
    data = d.listQuickfacts()
    d.dump(data, "list-quickfacts.json")
    data = d.listQuickfactsHashtag()
    d.dump(data, "list-quickfacts-hashtag.json")
    data = d.hashtagAll()
    d.dump(data, "hashtag-all.json")
    data = d.hashtagPairsAll()
    d.dump(data, "hashtagpairs-all.json")
    data = d.tableTweetImportant()
    d.dump(data, "table-tweet-important.json")
    data = d.tableTweetHashtagAll()
    d.dump(data, "table-tweet-hashtag-all.json")
    data = d.maphash2tweet()
    d.dump(data, "hash2tweet.json")
    data = d.maphash2hash()
    d.dump(data, "hash2hash.json")
    data = d.maptweet2hash()
    d.dump(data, "tweet2hash.json")
    data = d.networkHashtag()
    d.dump (data, "network-hashtag.json")
    data = d.networkHashtagCluster()
    d.dump(data, "network-hashtag-cluster.json")

if __name__ == '__main__':
    main()
