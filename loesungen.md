1. Aufgabe: -
2. Aufgabe:
    - 6126 Tweets von Donald Trump und Hillary Clinton vom 5.1.2016 - 28.9.2016
    - Stuktur: Autor, Tweet, Retweetstatus, Origial Autor, Uhrzeit, Quotestatus, Retweet Count, Favourite Count, Gerät, Status ob Tweet gekürzt ist
    - in Tweets: Mentions, Hashtags, Links

3. Aufgabe:
    - Beschreibung zum ER
        - Retweetstatus, Origial Autor, Quotestatus, Gerät, Truncate sind für Datenbank unwichtig
        - Tweet ID zur eindeutigen Idendifikation; Zeitpunkt, Autor, Retweets, Favourites und Text sind für die Auswertung wichtig
        - mit Relation enthällt Speichern wir, wie oft Hashtag vorkommt
        - Hashtags sind eindeutig, deswegen ist Text der Key
        - absoulte Anzahl an Hashtags ist aus der Relation "enthällt" und derem Attribut ableitbar
        - "tritt auf mit" ist aus den Rest Ableitbar

4. Aufgabe

    Unser relationelles Modell enthält die folgende Schemas:
    - Tweet(ID : int, Zeitpunkt : int, Autor : char[15], Retweets : int, Favourites : int, Text : char[140])
    - Hashtag(Text : [140], absoluteAnzahl : int)
    - enthällt(ID : int, Text : char[140], Anzahl : int)
    - trittAufMit(TextA : char[140], TextB : char[140], Anzahl : int)
    
    Die meiste von den verwendeten Datentypen sind ohne große Erklärungen verständlich. Zum Beispiel, es ist klar, dass die Anzahl der Retweets beziehungsweise der Favourites als ints betrachtet werden. Einige gewählte Datentypen ergeben sich aus den Spezifikationen und Regeln von Twitter. Benutzernamen dürfen nur 15 Zeichen lang sein [Quelle: https://support.twitter.com/articles/14609], deswegen ist Autor ein String von chars mit Länge 15. Tweets sind beschränkt auf 140 Zeichen, deshalb ist der gewählte Datentyp ein String von chars mit Länge 140. Logischerweise dürfen Hashtags nicht länger als ein Tweet sein und hat damit den gleichen Datentyp wie Tweets. Zusätzlich haben wir entschieden, dass der Zeitpunkt als UNIX-Timestamp dargstellt wird, da wir noch kein besseres Format kennen, um die Zeit zu Speichern, deshalb hat er den Datentyp Int.